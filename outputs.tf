output "deployer_role_policy_json" {
  description = "The deployer IAM role"
  value = data.aws_iam_policy_document.deployer_role_policy.json
}
